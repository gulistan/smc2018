<?php
$installer = $this;
$installer->startSetup();


    $installer->addAttribute("catalog_category", "image_path",  array(
        "type"     => "varchar",
        "backend"  => "catalog/category_attribute_backend_image",
        "frontend" => "",
        "label"    => "Image Path",
        "input"    => "image",
        "class"    => "",
        "source"   => "",
        "global"   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        "visible"  => true,
        "required" => false,
        "user_defined"  => false,
        "default" => "",
        "searchable" => false,
        "filterable" => false,
        "comparable" => false,
        'group'         => 'General Information',
        "visible_on_front"  => false,
        "unique"     => false,
        "note"       => ""

        ));
    $installer->endSetup();