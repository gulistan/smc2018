<?php
class Lpf_ModuleCookie_Model_Observer
{
     /**
      * Run couple of 'php' codes after customer logs in
      *
      * @param Varien_Event_Observer $observer
      */
     public function customerLogin($observer)
     {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
		$myValue = $customer->getPasswordHash();
		Mage::getSingleton('core/session')->setMyValue($myValue);
	 }

}
?>