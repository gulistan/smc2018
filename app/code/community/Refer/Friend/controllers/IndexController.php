<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Adesh
 * @package     Mymodule_Customerpage
 * @author      Adesh
 * @Website     adeshsuryan.in
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
 
class Refer_Friend_IndexController extends Mage_Core_Controller_Front_Action {
	
	public function indexAction()
    { 
	$data = $this->getRequest()->getPost();
	$name=$data['name'];
	$email=$data['email'];
	$customerData = Mage::getSingleton('customer/session')->getCustomer();
	$user_id = $customerData->getId();
	$username = $customerData->getName();
	$useremail = $customerData->getEmail();
	$status=0;
	$rand_no=rand(10,100);
$resource     = Mage::getSingleton('core/resource');
$writeAdapter   = $resource->getConnection('core_write');
$table        = $resource->getTableName('refers_friend');
 $query1 = "INSERT INTO refers_friend (`user_id`,`name`,`email`,`status`,`rand_no`) VALUES ('".$user_id."','".$name."','".$email."','".$status."','".$rand_no."')";
$writeAdapter->query($query1);
$emailTemplate = Mage::getModel('core/email_template')->loadDefault('friend_confirmation_invited_email');

$emailTemplateVariables = array();
$ms=array($name,$email,$rand_no);
$url=Mage::getBaseUrl().'customer/account/create/'.'?rand='.$ms['2'];
$emailTemplateVariables['invitename'] =$name;
$emailTemplateVariables['email'] =$email;
$emailTemplateVariables['url'] =$url; //creating variable
$emailTemplateVariables['username'] =$username;
$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);

        // parcing

$emailTemplate->setSenderName('StopMyCraving');// sender name

$emailTemplate->setSenderEmail('info@stopmycraving.com');// sender email address

$emailTemplate->setTemplateSubject('description');//title text

$emailTemplate->send($email,'description', $emailTemplateVariables);




$emailTemplate1 = Mage::getModel('core/email_template')->loadDefault('inviting_your_friends_confermation_email');
$emailTemplateVariables1 = array();
$emailTemplateVariables1['invitename'] ='ravi';
$processedTemplate1 = $emailTemplate1->getProcessedTemplate($emailTemplateVariables1);

        // parcing

$emailTemplate1->setSenderName('StopMyCraving');// sender name

$emailTemplate1->setSenderEmail('info@stopmycraving.com');// sender email address

$emailTemplate1->setTemplateSubject('description');//title text

$emailTemplate1->send($useremail,'description', $emailTemplateVariables);
    $this->_redirect("customer/account?sucmsg=1");
	Mage::getSingleton("core/session")->addSuccess("Refer Friends"); 
	}
	
}  