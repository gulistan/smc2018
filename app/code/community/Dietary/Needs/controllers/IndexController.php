<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Adesh
 * @package     Mymodule_Customerpage
 * @author      Adesh
 * @Website     adeshsuryan.in
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
 
class Dietary_Needs_IndexController extends Mage_Core_Controller_Front_Action {
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }      
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
		public function updatedirearyAction()		
		{
		$arra=$_POST['cat'];
				//print_r($arra);
				if(Mage::getSingleton('customer/session')->isLoggedIn()) {
					
					  $customerData = Mage::getSingleton('customer/session')->getCustomer();
					   $user_id = $customerData->getId();
					   $connection = Mage::getModel('core/resource')->getConnection('core_read');
						$sql = 'SELECT * FROM user_fav_cat where user_id='.$user_id;
						$favCat = $connection->fetchAll($sql);
						$already=array();
						if(!empty($favCat)){
							foreach($favCat as $cat){ 
								if(in_array($cat['cat_id'],$arra))
								{
								 $already[]=$cat['cat_id'];
								}else
								{
									 $sql2 = "DELETE FROM user_fav_cat WHERE cat_id=".$cat['cat_id'] ." AND user_id=".$user_id;
								    // echo $sql2;
									 $connection->query($sql2);
								}
							}
						}
					foreach($arra as $ids)
					{
						if(!in_array($ids,$already))
						{
						$query3 = "INSERT INTO user_fav_cat (`user_id`,`cat_id`) VALUES ($user_id,$ids);";
						//echo $query3;
						$connection->query($query3);
						}
					}
		Mage::getSingleton("core/session")->addSuccess("ADDED YOUR DIETARY NEEDS"); 
		$this->_redirect("dietary_needs/");		
		}		
	else
	{
		Mage::getSingleton("core/session")->addError("PLEASE SELECT YOUR DIETARY NEEDS"); 
		$this->_redirect("dietary_needs/");
	}
		
	
	}
	
	public function dietarydeleteAction()
	{
		$ids= $_GET['id'];
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
		$customerData = Mage::getSingleton('customer/session')->getCustomer();
		$user_id = $customerData->getId();
		$sql2 = "DELETE FROM user_fav_cat WHERE cat_id=".$ids ." AND user_id=".$user_id;
		try {
		$conn = Mage::getSingleton('core/resource')->getConnection('core_write');
		$conn->query($sql2);
		} catch (Exception $e){
	   //echo $e->getMessage();
		}
		}
		$this->_redirect('customer/account/');
	}
}  