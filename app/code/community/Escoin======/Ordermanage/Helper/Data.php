<?php 
/**
 * Escoin_Ordermanage extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Escoin
 * @package		Escoin_Ordermanage
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Ordermanage default helper
 *
 * @category	Escoin
 * @package		Escoin_Ordermanage
 * �(�
 */
class Escoin_Ordermanage_Helper_Data extends Mage_Core_Helper_Abstract{

	public function getCancel()
	{
		return Mage::getStoreConfig('ordermanage/frontend/orderstate');
	}

	public function isEnabled()
	{
		return Mage::getStoreConfig('ordermanage/frontend/enabled');
	}
}