<?php 
class Imedia_Quickview_IndexController extends Mage_Core_Controller_Front_Action{
    
	public function indexAction(){
        
        $params = $this->getRequest()->getParams();
        if(isset($params['id'])){
		echo $this->getLayout()->createBlock('core/template')->setTemplate('imedia/quickview/view.phtml')->toHtml();
        }else{  
          $this->_redirectReferer(); 
        }
 }

    public function discountAction() {
        $param = $this->getRequest()->getParams();
        if (isset($param['discount']) && ($param['discount'] == 'Weekly (20% off)')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(20);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('Weekly (20% off)');
        } else if (isset($param['discount']) && ($param['discount'] == 'Monthly (10% off)')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(10);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('Monthly (10% off)');
        } else if (isset($param['discount']) && ($param['discount'] == 'No thanks')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(0);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('No thanks');
        } else {
            Mage::getSingleton('core/session')->unsSubscribeDiscount();
            Mage::getSingleton('core/session')->unsSubscribeDiscountTitle();
        }
        echo json_encode(array('success' => TRUE));
        exit(0);
    }
	
	public function userchoiceAction(){
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
		$customerData = Mage::getSingleton('customer/session')->getCustomer();
		$user_id = $customerData->getId();	
		$data = $this->getRequest()->getPost();
		/****read***/
		
$connection = Mage::getModel('core/resource')->getConnection('core_read');
$sql = 'SELECT * FROM user_fav_cat where user_id='.$user_id;
$favCat = $connection->fetchAll($sql);
$already=array();
if(!empty($favCat)){
	foreach($favCat as $cat){
		$already[]=$cat['cat_id'];
	}
}
//print_r($favCat);exit;
		/****write*****/
		$resource     = Mage::getSingleton('core/resource');
		$writeAdapter   = $resource->getConnection('core_write');
		$table        = $resource->getTableName('user_fav_cat');
        if($data['cat']){
		 foreach($data['cat'] as $ids){
			 if(!in_array($ids,$already)){
    		  $query = "INSERT INTO user_fav_cat (`user_id`,`cat_id`) VALUES ($user_id,$ids);";
	    	  $writeAdapter->query($query);
			 }
		 }
		}
		$result=array('success'=>TRUE);
				 }
			 else{
				 
			 		$result=array('success'=>FALSE,'url'=>Mage::getUrl('customer/account'));
			 }			
	echo json_encode($result);exit;
	}
	
	public function myredirection(Varien_Event_Observer $observer) {
        $AccountController = $observer->getEvent()->getAccountController();

        $Customer = $observer->getEvent()->getCustomer();

         $response1 = Mage::app()->getResponse(); // observers have event args

            //set any url you want
            $url = Mage::getBaseUrl().'sucess'; //'http://www.example.com/';
            $response1->setRedirect($url);
            Mage::app()->getFrontController()->sendResponse();

        return;
      }
}
?>