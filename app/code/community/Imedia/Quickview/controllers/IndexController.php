<?php 
class Imedia_Quickview_IndexController extends Mage_Core_Controller_Front_Action{
    
	public function indexAction(){
        
        $params = $this->getRequest()->getParams();
        if(isset($params['id'])){
		echo $this->getLayout()->createBlock('core/template')->setTemplate('imedia/quickview/view.phtml')->toHtml();
        }else{  
          $this->_redirectReferer(); 
        }
 }
 public function exploreAction(){
        $block = $this->getLayout()->createBlock('core/template')->setTemplate('catalog/navigation/popuplogin.phtml');
		$html = $block->toHtml();
        echo $html;
 }
 public function loginAction(){
    
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {
            return;
        }

        $result = array(
            'success' => false
        );

        if ($this->getRequest()->isPost()) {
            $login = $this->getRequest()->getPost();
			$a=explode(",",$this->getRequest()->getPost('cat'));
			if (!empty($this->getRequest()->getPost('email')) && !empty($this->getRequest()->getPost('password'))) {
                try {
                    $session->login($this->getRequest()->getPost('email'), $this->getRequest()->getPost('password'));
                    $result['redirect'] = Mage::getUrl('customer/account', array('_secure' => true));
                    $result['success'] = true;
                } catch (Mage_Core_Exception $e) {
                    switch ($e->getCode()) {
                        case Mage_Customer_Model_Customer::EXCEPTION_EMAIL_NOT_CONFIRMED:
                            $message = Mage::helper('customer')->__('This account is not confirmed. <a href="%s">Click here</a> to resend confirmation email.', Mage::helper('customer')->getEmailConfirmationUrl($login['username']));
                            break;
                        case Mage_Customer_Model_Customer::EXCEPTION_INVALID_EMAIL_OR_PASSWORD:
                            $message = $e->getMessage();
                            break;
                        default:
                            $message = $e->getMessage();
                    }
                    $result['error'] = $message;
                    $session->setUsername($login['username']);
                } catch (Exception $e) {
                    //Mage::helper("ajaxlogin")->log("There has been an error during the login.");
                    // Mage::logException($e); // PA DSS violation: this exception log can disclose customer password
                }
            } 
			else 
			{
                $result['error'] = Mage::helper('customer')->__('Login and password are required.');
            }
			if(Mage::getSingleton('customer/session')->isLoggedIn()) {
				$customerData = Mage::getSingleton('customer/session')->getCustomer();
				$user_id = $customerData->getId();	
				$data = $this->getRequest()->getPost();
				/****read***/
				$connection = Mage::getModel('core/resource')->getConnection('core_read');
				$sql = 'SELECT * FROM user_fav_cat where user_id='.$user_id;
				$favCat = $connection->fetchAll($sql);
				$already=array();
				if(!empty($favCat)){
					foreach($favCat as $cat){
						$already[]=$cat['cat_id'];
					}
				}
				//print_r($favCat);//exit; 
						/****write*****/
				$resource     = Mage::getSingleton('core/resource');
				$writeAdapter   = $resource->getConnection('core_write');
				$table        = $resource->getTableName('user_fav_cat');
				if($a){
					 foreach($a as $ids){
						 if($ids!=''){
							 if(!in_array($ids,$already)){
								 Mage::log(' | query '.'INSERT INTO user_fav_cat (`user_id`,`cat_id`) VALUES ($user_id,$ids)',null,'query.log');
							  $query = "INSERT INTO user_fav_cat (`user_id`,`cat_id`) VALUES ($user_id,$ids);";
							  $writeAdapter->query($query);
							 }
						 }
					 }
				}
				//$result=array('success'=>TRUE);
			}
        }

       /*  $this->getResponse()
            ->setHeader('Access-Control-Allow-Origin', rtrim(Mage::getUrl(''),'/'))
            ->setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Origin, Access-Control-Allow-Headers, Access-Control-Allow-Credentials')
            ->setHeader('Access-Control-Expose-Headers', 'x-json')
            ->setHeader('Access-Control-Allow-Credentials', 'true')
            ->setBody(Mage::helper('core')->jsonEncode($result)); */
			echo json_encode($result);
    }
 
 public function setoptionsAction(){
        $data = $this->getRequest()->getPost();
		$cat= $this->getRequest()->getPost('cat');
		$i=1;
		foreach($cat as $c){
			Mage::getSingleton('core/session')->settotalselection(count($cat));
			$session = Mage::getSingleton("core/session",  array("name"=>"frontend"));
			// set data
			$session->setData('value'.$i, $c);
			$session->getData('value'.$i);
			$i++;
			
		}
        $result=array('success'=>TRUE);
		echo json_encode($result);//ssssexit;

 }

    public function discountAction() {
        $param = $this->getRequest()->getParams();
        if (isset($param['discount']) && ($param['discount'] == 'Weekly (20% off)')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(20);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('Weekly (20% off)');
        } else if (isset($param['discount']) && ($param['discount'] == 'Monthly (10% off)')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(10);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('Monthly (10% off)');
        } else if (isset($param['discount']) && ($param['discount'] == 'No thanks')) {
            Mage::getSingleton('core/session')->setSubscribeDiscount(0);
            Mage::getSingleton('core/session')->setSubscribeDiscountTitle('No thanks');
        } else {
            Mage::getSingleton('core/session')->unsSubscribeDiscount();
            Mage::getSingleton('core/session')->unsSubscribeDiscountTitle();
        }
        echo json_encode(array('success' => TRUE));
        exit(0);
    }
	
	public function userchoiceAction(){
		if(Mage::getSingleton('customer/session')->isLoggedIn()) {
		$customerData = Mage::getSingleton('customer/session')->getCustomer();
		$user_id = $customerData->getId();	
		$data = $this->getRequest()->getPost();
		/****read***/
		
$connection = Mage::getModel('core/resource')->getConnection('core_read');
$sql = 'SELECT * FROM user_fav_cat where user_id='.$user_id;
$favCat = $connection->fetchAll($sql);
$already=array();
if(!empty($favCat)){
	foreach($favCat as $cat){
		$already[]=$cat['cat_id'];
	}
}
//print_r($favCat);exit; 
		/****write*****/
		$resource     = Mage::getSingleton('core/resource');
		$writeAdapter   = $resource->getConnection('core_write');
		$table        = $resource->getTableName('user_fav_cat');
        if($data['cat']){
		 foreach($data['cat'] as $ids){
			 if(!in_array($ids,$already)){
    		  $query = "INSERT INTO user_fav_cat (`user_id`,`cat_id`) VALUES ($user_id,$ids);";
	    	  $writeAdapter->query($query);
			 }
		 }
		}
		$result=array('success'=>TRUE);
				 }
			 else{
				 
			 		$result=array('success'=>FALSE,'url'=>Mage::getUrl('customer/account'));
			 }			
	echo json_encode($result);exit;
	}
	
	public function myredirection(Varien_Event_Observer $observer) {
        $AccountController = $observer->getEvent()->getAccountController();

        $Customer = $observer->getEvent()->getCustomer();

         $response1 = Mage::app()->getResponse(); // observers have event args

            //set any url you want
            $url = Mage::getBaseUrl().'sucess'; //'http://www.example.com/';
            $response1->setRedirect($url);
            Mage::app()->getFrontController()->sendResponse();

        return;
      }
}
?>