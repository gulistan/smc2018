<?php
$var = "<?xml version='1.0' encoding='UTF-8'?>
<p:DCTRequest xmlns:p='http://www.dhl.com' xmlns:p1='http://www.dhl.com/datatypes' xmlns:p2='http://www.dhl.com/DCTRequestdatatypes' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.dhl.com DCT-req.xsd'>
  <GetQuote>
    <Request>
      <ServiceHeader>
        <MessageTime>2017-08-02T11:28:56.000-08:00</MessageTime>
        <MessageReference>1234567890123456789012345678901</MessageReference>
        <SiteID>CIMGBTest</SiteID>
        <Password>DLUntOcJma</Password>
      </ServiceHeader>
    </Request>
    <From>
      <CountryCode>SG</CountryCode>
      <Postalcode>247964</Postalcode>
    </From>
    <BkgDetails>
      <PaymentCountryCode>SG</PaymentCountryCode>
      <Date>2017-08-22</Date>
      <ReadyTime>PT10H21M</ReadyTime>
      <ReadyTimeGMTOffset>+01:00</ReadyTimeGMTOffset>
      <DimensionUnit>CM</DimensionUnit>
      <WeightUnit>KG</WeightUnit>
      <Pieces>
        <Piece>
          <PieceID>1</PieceID>
          <Height>1</Height>
          <Depth>1</Depth>
          <Width>1</Width>
          <Weight>5.0</Weight>
        </Piece>
      </Pieces> 
	  <PaymentAccountNumber>CASHSIN</PaymentAccountNumber>	  
      <IsDutiable>N</IsDutiable>
      <NetworkTypeCode>AL</NetworkTypeCode>
	  <QtdShp>
		 <GlobalProductCode>D</GlobalProductCode>
	     <LocalProductCode>D</LocalProductCode>		
	     <QtdShpExChrg>
            <SpecialServiceType>AA</SpecialServiceType>
         </QtdShpExChrg>
	  </QtdShp>
    </BkgDetails>
    <To>
      <CountryCode>AU</CountryCode>
      <Postalcode>0872</Postalcode>
    </To>
   <Dutiable>
      <DeclaredCurrency>EUR</DeclaredCurrency>
      <DeclaredValue>1.0</DeclaredValue>
    </Dutiable>
  </GetQuote>
</p:DCTRequest>";
//step1
$cSession = curl_init(); 
//step2
curl_setopt($cSession,CURLOPT_URL,"https://xmlpitest-ea.dhl.com/XMLShippingServlet");
curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
curl_setopt($cSession,CURLOPT_HEADER, false); 
curl_setopt($cSession, CURLOPT_POST, 1);
curl_setopt($cSession, CURLOPT_POSTFIELDS,  $var);
//step3
$data=curl_exec($cSession);
//step4
 $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

        print_r('<pre>');
        print_r($array_data);
        print_r('</pre>');
curl_close($cSession);
//step5

?>
