<?php //valid10
$var = '<?xml version="1.0" encoding="UTF-8"?>
<p:DCTRequest xmlns:p="http://www.dhl.com" xmlns:p1="http://www.dhl.com/datatypes" xmlns:p2="http://www.dhl.com/DCTRequestdatatypes" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dhl.com DCT-req.xsd ">
  <GetCapability>
    <Request>
      <ServiceHeader>
        <MessageTime>2017-08-02T11:28:56.000-08:00</MessageTime>
        <MessageReference>1234567890123456789012345678901</MessageReference>
        <SiteID>CIMGBTest</SiteID>
        <Password>DLUntOcJma</Password>
      </ServiceHeader>
    </Request>
    <From>
      <CountryCode>RU</CountryCode>
      <Postalcode>125167</Postalcode>
	  <City>central</City>
    </From>
    <BkgDetails>
      <PaymentCountryCode>RU</PaymentCountryCode>
      <Date>2017-08-02</Date>
      <ReadyTime>PT10H21M</ReadyTime>
      <ReadyTimeGMTOffset>+01:00</ReadyTimeGMTOffset>
      <DimensionUnit>CM</DimensionUnit>
      <WeightUnit>KG</WeightUnit>
      <Pieces>
        <Piece>
          <PieceID>1</PieceID>
          <Height>30</Height>
          <Depth>20</Depth>
          <Width>10</Width>
          <Weight>1.0</Weight>
        </Piece>
      </Pieces>      
      <IsDutiable>N</IsDutiable>
      <NetworkTypeCode>AL</NetworkTypeCode>
	 <QtdShp>
      <QtdShpExChrg>
        <SpecialServiceType>OSINFO</SpecialServiceType>
      </QtdShpExChrg>
    </QtdShp>
    </BkgDetails>
    <To>
      <CountryCode>RU</CountryCode>
      <Postalcode>620085</Postalcode>
    </To>
   <Dutiable>
      <DeclaredCurrency>USD</DeclaredCurrency>
      <DeclaredValue>1002.00</DeclaredValue>
    </Dutiable>
  </GetCapability>
</p:DCTRequest>';
//step1
$cSession = curl_init(); 
//step2
curl_setopt($cSession,CURLOPT_URL,"https://xmlpitest-ea.dhl.com/XMLShippingServlet");
curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
curl_setopt($cSession,CURLOPT_HEADER, false); 
curl_setopt($cSession, CURLOPT_POST, 1);
curl_setopt($cSession, CURLOPT_POSTFIELDS,  $var);
//step3
$data=curl_exec($cSession);
//step4
 $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

        print_r('<pre>');
        print_r($array_data);
        print_r('</pre>');
curl_close($cSession);
//step5

?>
